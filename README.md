# building a ranking

Crée un ranking de page à partir d'un index et de la liste de ces pages.

## Install

```
git clone https://gitlab.com/lpbb/building-a-ranking.git
cd building-a-ranking
pip install -r requirements.txt
```

## Launch

```
python3 main.py index_path documents_path query
```

## Return

`results.json` comprenant le ranking des pages correspondantes au query ainsi que le nombre de pages et le nombre de token dans l'index.

## Tests

Pour lancer les tests :

```
python3 -m unittest test/test.py
```

## Author

Perraud Louis
