from ranking.ranking_engine import RankingEngine
import unittest
import json

class TestRankingEngine(unittest.TestCase):
    def setUp(self):
        # Create a sample index file and documents file
        index = {
            'test': {
                1: {'count': 2},
                2: {'count': 3}
            },
            'search': {
                1: {'count': 3},
                2: {'count': 2}
            }
        }
        documents = [
            {'id': 1, 'title': 'Test Document 1', 'url': 'www.test1.com'},
            {'id': 2, 'title': 'Test Document 2', 'url': 'www.test2.com'}
        ]
        
        with open('index.json', 'w') as f:
            json.dump(index, f)
            
        with open('documents.json', 'w') as f:
            json.dump(documents, f)
            
        # Create the RankingEngine object
        self.engine = RankingEngine('index.json', 'documents.json', 'test search')
    
    def tearDown(self):
        # Delete the sample index and documents files
        import os
        os.remove('index.json')
        os.remove('documents.json')
    
    def test_search(self):
        # Check that the search returns the expected result
        expected = {
            'results': [
                {'title': 'Test Document 1', 'url': 'www.test1.com'},
                {'title': 'Test Document 2', 'url': 'www.test2.com'}
            ],
            'index_size': 2,
            'results_size': 2
        }
        self.assertEqual(self.engine.ranking, expected)

