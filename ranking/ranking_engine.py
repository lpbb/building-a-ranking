import json
from collections import defaultdict
from nltk.tokenize import word_tokenize

class RankingEngine:
    def __init__(self, index_file, documents_file, query):
        self.index = self.load_index(index_file)
        self.documents = self.load_documents(documents_file)
        self.doc_count = len(self.documents)
        self.ranking = self.search(query)
    
    def load_index(self, index_file):
        with open(index_file) as f:
            return json.load(f)
        
    def load_documents(self, documents_file):
        with open(documents_file) as f:
            return {doc['id']: doc for doc in json.load(f)}
        
    def tokenize_query(self, query):
        all_tokens =word_tokenize(query.lower())
        
        tokens_in_index = []
        for token in all_tokens:
            if token in self.index.keys():
                tokens_in_index.append(token)
        return tokens_in_index
        
    def get_matching_docs(self, query):
        tokens = self.tokenize_query(query)
        if len(tokens) :
            matching_docs = set(self.index[tokens[0]].keys())  
            for token in tokens:
                try:
                    matching_docs &= set(self.index[token].keys())
                except:
                    continue
        else :
            matching_docs = set()
        return matching_docs
        
    
    def linear_ranking(self, matching_docs, tokens):
        scores = defaultdict(int)
        for doc_id in matching_docs:
            for token in tokens:
                scores[doc_id] += self.index[token][doc_id]['count']
        return sorted(scores.items(), key=lambda x: x[1], reverse=True)
    
    def save_ranking(self, ranking_file) -> None:
        json.dump(self.ranking, open(ranking_file, "w"))

    def search(self, query):
        matching_docs = self.get_matching_docs(query)
        tokens = self.tokenize_query(query)
        ranked_docs = self.linear_ranking(matching_docs, tokens)
        result = [{'title': self.documents[int(doc_id)]['title'], 'url': self.documents[int(doc_id)]['url']} for doc_id, score in ranked_docs]
        
        return {'results': result, 'index_size': self.doc_count, 'results_size': len(result)}




