from ranking.ranking_engine import RankingEngine
import argparse



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("index_path", help="the PATH of the index")
    parser.add_argument("documents_path", help="the PATH of the documents")
    parser.add_argument("query", help="the query")
    args = parser.parse_args()
    search_engine = RankingEngine(args.index_path, args.documents_path, args.query)
    search_engine.save_ranking("results.json")
    
   